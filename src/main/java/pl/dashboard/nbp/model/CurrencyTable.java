package pl.dashboard.nbp.model;

import java.util.List;

public class CurrencyTable {
    private List<Rate> rates;

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(List<Rate> rates) {
        this.rates = rates;
    }
}
