package pl.dashboard.nbp.service;

import pl.dashboard.nbp.exception.AppException;
import pl.dashboard.nbp.model.CurrencyTable;
import pl.dashboard.nbp.model.Rate;
import retrofit2.Response;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class NBPRatesService {

    private static final DateTimeFormatter API_DATE_FORMAT = DateTimeFormatter.ISO_DATE;
    private static final String IO_EXCEPTION = "Wyjątek we/wy, sprawdź połączenie z internetem";
    private static final String UNKNOWN_HTTP_CODE = "Nieobsługiwany kod http: ";
    private static final String RATES_NOT_FOUND = "Nie znaleziono kursów walut dla wybranej daty.";
    private static final LocalDate BEGINNING_OF_NBP_RATES = LocalDate.of(2002, 1, 1);
    private final NBPWebClient nbpWebClient;

    public NBPRatesService(NBPWebClient nbpWebClient) {
        this.nbpWebClient = nbpWebClient;
    }

    public List<Rate> getRates(LocalDate date) {

        LocalDate now = LocalDate.now();
        if (now.isBefore(date) && date.isAfter(BEGINNING_OF_NBP_RATES)) {
            throw new AppException(String.format("Nieobsługiwana data - obsługiwany zakres dat to: %s do %s",
                    BEGINNING_OF_NBP_RATES.plusDays(1).format(DateTimeFormatter.ISO_DATE),
                    LocalDate.now().format(DateTimeFormatter.ISO_DATE)));
        }


        final String apiFormattedDate = date.format(API_DATE_FORMAT);
        try {
            final Response<List<CurrencyTable>> rsp = nbpWebClient.get(apiFormattedDate).execute();

            if (rsp.code() == 404) {
                throw new AppException(RATES_NOT_FOUND);
            } else if (!rsp.isSuccessful()) {
                throw new AppException(UNKNOWN_HTTP_CODE + rsp.code() + " " + rsp.raw().message());
            }
            return rsp.body().get(0).getRates();
        } catch (IOException ex) {
            throw new AppException(IO_EXCEPTION, ex);
        }
    }
}
