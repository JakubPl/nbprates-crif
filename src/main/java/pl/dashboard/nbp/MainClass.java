package pl.dashboard.nbp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import pl.dashboard.nbp.exception.AppException;
import pl.dashboard.nbp.model.Rate;
import pl.dashboard.nbp.service.NBPRatesService;
import pl.dashboard.nbp.service.NBPWebClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MainClass {

    private static final DateTimeFormatter OUTPUT_DATE_FORMAT = DateTimeFormatter.ofPattern("MM.dd.yyyy");
    private static final DateTimeFormatter INPUT_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final String INVALID_DATE_FORMAT = "Niepoprawny format daty, poprawny format daty to: yyyy-MM-dd.";
    private static final List<String> CURRENCIES = Arrays.asList("EUR", "CHF", "USD", "GBP");
    private static final String NO_ARG_SUPPLIED = "Przekaż datę w formacie: yyyy-MM-dd, np. 2013-12-13";
    private static final String RATE_PRINT_ROW = "Data: %s\nWaluta = kupno; sprzedaż\n%s%n";

    public static void main(String[] args) {
        try {
            final LocalDate date = parseAndValidateInput(args);

            NBPRatesService nbpRatesService = new NBPRatesService(createNBPWebClient());

            final Map<String, Rate> ratesMap = getCurrencyRateMap(date, nbpRatesService.getRates(date));

            String ratesString = CURRENCIES.stream()
                    .map(curr -> getCurrenyString(ratesMap, curr))
                    .collect(Collectors.joining("\n"));

            System.out.printf(RATE_PRINT_ROW, date.format(OUTPUT_DATE_FORMAT), ratesString);
        } catch (AppException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static LocalDate parseAndValidateInput(String[] args) {
        if (args.length != 1) {
            throw new AppException(NO_ARG_SUPPLIED);
        }
        return parseDate(args);
    }

    private static Map<String, Rate> getCurrencyRateMap(LocalDate date, List<Rate> rates) {
        return rates.stream()
                .collect(Collectors.toMap(Rate::getCode, v -> v));
    }

    private static LocalDate parseDate(String[] args) {
        try {
            return LocalDate.parse(args[0], INPUT_DATE_FORMAT);
        } catch (DateTimeException ex) {
            throw new AppException(INVALID_DATE_FORMAT, ex);
        }
    }

    private static String getCurrenyString(Map<String, Rate> ratesString, String currency) {
        Rate rate = ratesString.get(currency);
        return String.format("%s = %s; %s", currency, rate.getBid(), rate.getAsk());
    }


    private static NBPWebClient createNBPWebClient() {
        return new Retrofit.Builder()
                .baseUrl("http://api.nbp.pl/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(NBPWebClient.class);
    }
}
