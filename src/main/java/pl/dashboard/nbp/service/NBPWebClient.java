package pl.dashboard.nbp.service;

import pl.dashboard.nbp.model.CurrencyTable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

public interface NBPWebClient {
    @GET("exchangerates/tables/c/{date}/?format=json")
    Call<List<CurrencyTable>> get(@Path("date") String date);
}
